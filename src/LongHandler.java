public class LongHandler extends NumberHandler<Long> {
    @Override
    protected void handleNumber(Long aLong) {
        System.out.println(aLong);
    }
}
