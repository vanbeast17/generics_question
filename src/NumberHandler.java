public abstract class NumberHandler<T extends Number> {
    public void handle(String numbertype){
        SupplyHelper helper = new SupplyHelper();
        T data = helper.getNumber(numbertype);
        handleNumber(data);
    }

    protected abstract void handleNumber(T t);
}
