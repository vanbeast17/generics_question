public class DoubleHandler extends NumberHandler<Double> {
    @Override
    protected void handleNumber(Double aDouble) {
        System.out.println(aDouble);
    }
}
